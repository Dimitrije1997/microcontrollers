import sys
import matplotlib
import collections
import socket
import threading
import time
import math

matplotlib.use('Qt5Agg')

from PyQt5 import QtCore, QtWidgets

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

#velicina bafera za smjestanje podataka
BUFF_SIZE = 30

#interval uzimanja podataka
TIMER_T = 100

#IP adresa i port
IP = '10.99.128.2'
PORT = 10001

#Cirkularni baferi za rad sa podacima koje primamo sa servera
temp_values = collections.deque([0]*BUFF_SIZE, maxlen=BUFF_SIZE)
hum_values = collections.deque([0]*BUFF_SIZE, maxlen=BUFF_SIZE)


class MplCanvas(FigureCanvas):

    def __init__(self, parent=None, width=200, height=200, dpi=100):
        fig,axes = matplotlib.pyplot.subplots(2)
        matplotlib.pyplot.close(fig)
        self.axes = axes
        super(MplCanvas, self).__init__(fig)
        

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.canvas = MplCanvas(self, width=200, height=200, dpi=100)
        self.setCentralWidget(self.canvas)

        #odmjerci na x osi za iscrtavanje
        self.xdata = list(range(BUFF_SIZE))
        
        self.update_plot()

        self.show()

        # Postavljanje tajmera za update
        self.timer = QtCore.QTimer()
        self.timer.setInterval(TIMER_T)
        self.timer.timeout.connect(self.update_plot)
        self.timer.start()

    def update_plot(self):
        #ciscenje osa
        self.canvas.axes[0].cla()
        self.canvas.axes[1].cla()
        
        #crtanje novih podataka
        self.canvas.axes[0].plot(self.xdata, hum_values, 'r')
        self.canvas.axes[1].plot(self.xdata, temp_values, 'b')
        
        self.canvas.axes[0].set_ylabel('Humidity [%]',fontsize=16)
        self.canvas.axes[0].set_xlabel('Time [s]',fontsize=16)
        
        self.canvas.axes[1].set_ylabel('Temperature [°C]',fontsize=16)
        self.canvas.axes[1].set_xlabel('Time [s]',fontsize=16)
        
        # Precrtavanje grafika novim vrijednostima
        self.canvas.draw()
        

def consumer_thread():
    try:
        while(True):
            recieved,_ = udp_socket.recvfrom(BUFF_SIZE)
            text=recieved.decode('utf8')
            
            #parsiranje vrijednosti
            values=text.split()
            
            print("Temperatura i vlaznost vazduha:")
            temp=(int(values[0])/(math.pow(2,14)-1))*165-40
            temp_values.append(temp)
            
            hum=(int(values[1])/(math.pow(2,14)-1))*100
            hum_values.append(hum)
            
            print(temp)
            print(hum)
            
            time.sleep(0.5)
    finally:
        print("Zatvaranje soketa")
        udp_socket.close()


app = QtWidgets.QApplication(sys.argv)
w = MainWindow()

udp_socket = socket.socket(family=socket.AF_INET,type=socket.SOCK_DGRAM)
udp_socket.bind((IP,PORT))

server = threading.Thread(target=consumer_thread,daemon=True)
server.isBackround = True
server.start()

app.exec_()
udp_socket.close()