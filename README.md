Ovaj projekat predstavlja projektni zadataka iz predmeta Mikrokontrolerski sistemi koji sam radio na četvrtoj godini studija.
Tema projektnog zadatka je očitavanje podataka sa senzora i njihovo slanje pomoću PIC mikrokontrolera i eksternog Ethernet kontrolera
na personalni računar gdje se grafički prikazuju u jednostavnoj aplikaciji pisanoj u Python programskom jeziku.
