#include <stdint.h>

//definisanje pinova za Ethrenet srijsku plocu
sfr sbit SPI_Ethernet_Rst at RD5_bit;
sfr sbit SPI_Ethernet_CS  at RD3_bit;
sfr sbit SPI_Ethernet_Rst_Direction at TRISD5_bit;
sfr sbit SPI_Ethernet_CS_Direction  at TRISD3_bit;

//Definisanje parametara komunikacije: MAC i IP adrese
unsigned char MACAddr[6] = {0x00, 0x14, 0xa5, 0x76, 0x19, 0x3f} ;
unsigned char IPAddr[4] = {10,99,128,3};
unsigned char DestIpAddr[4]  = {10,99,128,2};

//bafer za slanje podataka
unsigned char txt[50];

//broj bajtova za slanje
int num_to_send = 0;

//promjenjive za skladistenje ocitavanja sa senzora
uint16_t light_value = 0;
uint16_t temp_value = 0;
uint16_t hum_value = 0;
uint16_t air_co2 = 0;
uint16_t air_voc = 0;

//pomocne promjenjive za citanje vrijednosti reg
uint16_t msb = 0;
uint16_t lsb = 0;

uint16_t msb_pom = 0;
uint16_t lsb_pom = 0;


typedef struct
{
  unsigned canCloseTCP:1;
  unsigned isBroadcast:1;
}TethPktFlags;

//Funkcija ciju definiciju zahtjeva SPI_Ethrenet biblioteka
unsigned int SPI_Ethernet_UserTCP(unsigned char *remoteHost,unsigned int remotePort,
                                  unsigned int localPort, unsigned int reqLength, TEthPktFlags *flags)
{
   return(0);
}

//Funkcija ciju definiciju zahtjeva SPI_Ethrenet biblioteka
unsigned int SPI_Ethernet_UserUDP(unsigned char *remoteHost, unsigned int remotePort,
                                  unsigned int destPort,unsigned int reqLength, TEthPktFlags *flags)
{
     return 0;
}

//Funkcija vrsi ocitavanje intenziteta osvjetljenja
//Sedmobitna adresa uredjaja je 0x29
void read_ambient_light()
{
        I2C1_Start();
        I2C1_Wr(0x52);
        I2C1_Wr(0x04);
        I2C1_Repeated_Start();
        I2C1_Wr(0x53);
        msb = I2C1_Rd(1);
        lsb = I2C1_Rd(0);
        I2C1_Stop();

        light_value = ((msb << 8) | lsb);
}

//Funkcija vrsi prebacivanje air quality senzora iz boot u app mod
void boot_to_app()
{
        I2C1_Start();
        I2C1_Wr(0xB4);
        I2C1_Wr(0xf4);
        I2C1_Wr(0x00);
        I2C1_Stop();
}

//Funkcija ocitava vrijednosti temperature i vlaznosti vazduha
//Sedmobitna I2C adresa uredjaja je 0x44
void read_temp_and_hum()
{
        RA0_bit=1;
     //Izlazak iz sleep moda
      I2C1_Start();
      I2C1_Wr(0x88);
      I2C1_Stop();

      //Cekanje da se izvrsi mjerenje
      Delay_ms(40);

      //Citanje izmjerenih rezultata
      I2C1_Start();
      I2C1_Wr(0x89);

      //vlaznost vazduha
      msb = I2C1_Rd(1);
      lsb = I2C1_Rd(1);
      
      //temperatura
      msb_pom = I2C1_Rd(1);
      lsb_pom = I2C1_Rd(0);

      I2C1_Stop();
      
      //formiranje temperature
      temp_value = (((msb_pom<<8) | (lsb_pom & 0xfffc))>>2);
      
      //formiranje vlaznosti vazduha
      hum_value = (((msb & 0x003f)<<8) | lsb);
      RA1_bit=1;
}

//Funkcija ocitava CO2 i VOC iz vazduha
//Sedmobitna adresa uredjaja je 0x5A, addr pin je prespojen na 0
void read_air_quality()
{
      //Postavljanje wake pina na log nulu
      RE0_bit=0;
      
      //Selekcija registra
      I2C1_Start();
      I2C1_Wr(0xB4);
      I2C1_Wr(0x02);
      I2C1_Stop();
      
      //Citanje CO2
      I2C1_Start();
      I2C1_Wr(0xB5);
      
      msb = I2C1_Rd(1);
      lsb = I2C1_Rd(0);
      
      air_co2 = ((msb << 8) | lsb);
      
      I2C1_Stop();
      
      //Citanje VOC
      I2C1_Start();
      I2C1_Wr(0xB5);

      msb = I2C1_Rd(1);
      lsb = I2C1_Rd(0);

      air_voc = ((msb << 8) | lsb);

      I2C1_Stop();
      
      //Postavljanje wake pina na log jedinicu (gasenje)
      RE0_bit=1;
}

//Funkcija inicijalizuje periferije sistema i portove
void system_init()
{
     //postavljanje portova na digitalne
     ANSELC = 0;
     ANSELA = 0;
     ANSELB = 0;
     ANSELE = 0;
     ANSELD = 0;

     //postavljanje smijera pinova i ciscenje portova
     TRISA = 0;
     PORTA = 0;

     TRISB = 0;
     PORTB = 0;

     TRISE = 0;
     PORTE = 0;

     //Inicijalizacija SPI-a MSSP2 periferije
     SPI2_Init();

     //Inicijalizacija I2C-a MSSP1 periferije
     I2C1_Init(100000);

     //Inicijalizacija Ethrenret kontrolera, arg 0x01 oznaca half-duplex
     SPI_Ethernet_Init(MACAddr, IPAddr, 0x01);

     //boot_to_up();
}

void main()
{
     //inicijalizacija sistema
     system_init();
     Delay_ms(3000);
     while(1)
     {
        //citanje vrijednosti sa senzora
        read_temp_and_hum();
        
        //read_ambient_light();
        //read_air_quality();
        //num_to_send = sprintf(txt,"%u %u %u",temp_value,hum_value,light_value);
        
        //upis procitanih vrijednosti u bafer za slanje
        num_to_send = sprintf(txt,"%u %u",temp_value,hum_value);
        
        //slanje podataka Ethrenet komunikacijom
        SPI_Ethernet_sendUDP(DestIpAddr, 10001, 10001, txt, num_to_send);
        
        Delay_ms(500);
     }
}